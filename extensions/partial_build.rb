# frozen_string_literal: true

require 'middleman'

#
# This middleman extension is used to speed up builds on CI
#
# We effectively do this by leveraging our CI parallelism features and split the build into six
# distinct partitions:
#
# 1. `PROXY_RESOURCE`: We leverage the `proxy` feature of middleman to generate e.g. job pages,
#    direction pages and others
# 2. `IMAGES`: We have a lot of images. So we let middleman copy them. Maybe we can de-middleman
#    it in the future
# 3. `BLOG_POST`: All blog posts
# 4. `HANDBOOK`: The handbook
# 5. `ASSETS`: javascripts, stylesheets, icons, and pdfs
# 6. `ALL_OTHERS`: All pages and files which do not fit into the categories above
#
# If `CI_NODE_INDEX` and `CI_NODE_TOTAL` are not set, e.g. on development machines, or turning
# parallelism off, it will simply be a noop extension
class PartialBuild < Middleman::Extension
  PROXY_RESOURCE = "proxy resources"
  IMAGES = "IMAGES"
  BLOG_POST_OLD = "blog posts old (up to 2016)"
  BLOG_POST_NEW = "blog posts new (since 2017)"
  HANDBOOK = "handbook"
  ASSETS = "javascript, stylesheets, icons, pdfs"
  ALL_OTHERS = "all other pages"

  # We must ensure that this extension runs last, so that
  # the filtering works correctly
  self.resource_list_manipulator_priority = 1000

  def_delegator :@app, :logger

  def initialize(app, options_hash = {}, &block)
    super
    @enabled = ENV['CI_NODE_INDEX'] && ENV['CI_NODE_TOTAL'] || ENV['CI_BUILD_PROXY_RESOURCE']

    if ENV['CI_BUILD_PROXY_RESOURCE']
      @partial = PROXY_RESOURCE
      return
    end

    return unless @enabled

    raise "PartialBuild: If you want to enable parallel builds, please use exactly 6 parallel jobs" unless ENV['CI_NODE_TOTAL'].to_i == 6

    @partial = case ENV['CI_NODE_INDEX']
               when "1"
                 IMAGES
               when "2"
                 BLOG_POST_OLD
               when "3"
                 BLOG_POST_NEW
               when "4"
                 HANDBOOK
               when "5"
                 ASSETS
               when "6"
                 ALL_OTHERS
               else
                 raise "PartialBuild: Invalid Build Partial #{ENV['CI_NODE_INDEX']}. At the moment we only support 1 to 6"
               end
  end

  def images?(resource)
    !proxy_resource?(resource) &&
      resource.destination_path.start_with?(
        'images/',
        'devops-tools/azure_devops/images/'
      )
  end

  def handbook?(resource)
    !proxy_resource?(resource) &&
      resource.destination_path.start_with?(
        'handbook/'
      )
  end

  def assets?(resource)
    !proxy_resource?(resource) &&
      resource.destination_path.start_with?(
        'ico/',
        'stylesheets/',
        'javascripts/',
        '/devops-tools/pdfs/',
        '/pdfs'
      )
  end

  def blog_page_new?(resource)
    !proxy_resource?(resource) &&
      ((resource.destination_path.start_with?('blog/') && !blog_page_old?(resource)) ||
        resource.destination_path.end_with?('atom.xml'))
  end

  def blog_page_old?(resource)
    !proxy_resource?(resource) &&
      resource.destination_path.start_with?(
        'blog/2011',
        'blog/2012',
        'blog/2012',
        'blog/2013',
        'blog/2014',
        'blog/2015',
        'blog/2016'
      )
  end

  def proxy_resource?(resource)
    resource.instance_of?(Middleman::Sitemap::ProxyResource) ||
      resource.destination_path.start_with?('templates/', 'direction/', 'sales/') ||
      resource.destination_path.end_with?('/template.html', 'category.html')
  end

  def all_others?(resource)
    !proxy_resource?(resource) &&
      !blog_page_new?(resource) &&
      !blog_page_old?(resource) &&
      !images?(resource) &&
      !handbook?(resource) &&
      !assets?(resource)
  end

  def part_of_partial?(resource)
    return true if resource.destination_path == 'sitemap.xml'

    case @partial
    when PROXY_RESOURCE
      proxy_resource?(resource)
    when IMAGES
      images?(resource)
    when BLOG_POST_OLD
      blog_page_old?(resource)
    when BLOG_POST_NEW
      blog_page_new?(resource)
    when HANDBOOK
      handbook?(resource)
    when ASSETS
      assets?(resource)
    when ALL_OTHERS
      all_others?(resource)
    else
      raise "PartialBuild: You are trying to build a unknown partial: #{@partial}"
    end
  end

  def manipulate_resource_list(resources)
    unless @enabled
      logger.info "PartialBuild: We are not building a partial build, building everything"
      return resources
    end

    logger.info "PartialBuild: We are building the partial: #{@partial}"

    resources.select { |resource| part_of_partial?(resource) }
  end
end

::Middleman::Extensions.register(:partial_build, PartialBuild)
